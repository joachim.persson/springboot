package com.example.springboot;

import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binomialName;
}
