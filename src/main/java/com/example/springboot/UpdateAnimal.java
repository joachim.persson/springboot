package com.example.springboot;

import lombok.Value;

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
